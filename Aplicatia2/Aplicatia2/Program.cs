﻿using System;
using System.Linq;

namespace Aplicatia2
{
    class Program
    {
        
        static void Main(string[] args)
        {
            string fullName;
            fullName = Console.ReadLine();
            int index = fullName.IndexOf(' ');
            string firstName = fullName.Substring(0, index);
            string lastName = fullName.Substring(index + 1);
            

            if (!char.IsUpper(firstName, 0))
            {
                
                firstName = firstName.First().ToString().ToUpper() + firstName.Substring(1); 
                
            }
            if (!char.IsUpper(lastName, 0))
            { 
                lastName = lastName.First().ToString().ToUpper() + lastName.Substring(1);

            }
           
            fullName = firstName + " " + lastName;
            Console.WriteLine(fullName);
        }
    }
}