﻿using System;

namespace Aplicatia3
{
    class LungimeA
    {
        public static bool PrimulSauUltimul(string[] a, string[] b, int i, int j)
        {
            if (a[0] == b[0] || a[i - 1] == b[j - 1])
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        static void Main(string[] args)
        {
            int i, j;     
                    
            Console.WriteLine("Lungimea multimii a este: ");
            i = Convert.ToInt32(Console.ReadLine());

            string[] a = new string[i];

            for (i = 0; i < a.Length; i++)
            {
                Console.WriteLine("a[" + i + "] = ");
                a[i] = Console.ReadLine();
            }

            Console.WriteLine("Lungimea multimii b este: ");
            j = Convert.ToInt32(Console.ReadLine());

            string[] b = new string[j];

            for (j = 0; j < b.Length; j++)
            {
                Console.WriteLine("b[" + j + "] = ");
                b[j] = Console.ReadLine();
            }

            Console.WriteLine("a = " + string.Join(",", a));
            Console.WriteLine("b = " + string.Join(",", b));

            if (PrimulSauUltimul(a, b, i, j))
            {
                Console.WriteLine("ok");
            }
            else
            {
                Console.WriteLine("ko");
            }
        }
    }        
}











